#ifndef ENEMY_H
#define ENEMY_H

#include "character.h"

class Enemy : public Character {
public:
    Enemy(const std::string& name, int health) : Character(name, health) {}

    void attack(Character* target) override {
        std::cout << name << " fiercely attacks " << target->getName() << "!" << std::endl;
        target->takeDamage(15); // Example damage
    }
};

#endif
