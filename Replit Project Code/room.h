#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "item.h"
#include "character.h" 

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
    std::vector<Character*> enemies; // For enemy handling

public:
    Room(const std::string& desc) : description(desc) {}

    void setDescription(const std::string& desc) { description = desc; }
    std::string getDescription() const { return description; }

    void addExit(const std::string& direction, Room* room) { exits[direction] = room; }
    Room* getExit(const std::string& direction) {
        auto search = exits.find(direction);
        if (search != exits.end()) {
            return search->second;
        } else {
            return nullptr;
        }
    }

    void addItem(const Item& item) { items.push_back(item); }
    std::vector<Item> getItems() const { return items; }

    // Enemy handling methods
    void addEnemy(Character* enemy) { enemies.push_back(enemy); }
    void removeEnemy(Character* enemy) {
        enemies.erase(std::remove(enemies.begin(), enemies.end(), enemy), enemies.end());
    }
    std::vector<Character*> getEnemies() const { return enemies; }
};

#endif
