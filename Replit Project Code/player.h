
#ifndef PLAYER_H
#define PLAYER_H

#include "character.h"
#include "room.h"

class Player : public Character {
private:
    Room* location;

public:
    Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}
    
    void setLocation(Room* newLocation) { location = newLocation; }
    Room* getLocation() const { return location; }
};

#endif
