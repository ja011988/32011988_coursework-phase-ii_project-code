#ifndef CHARACTER_H
#define CHARACTER_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm> 
#include "item.h"

class Character {
protected:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health) : name(name), health(health) {}

    virtual void takeDamage(int damage) {
        health -= damage;
        if (health < 0) health = 0;
    }

    void addItem(const Item& item) { inventory.push_back(item); }

    bool hasItem(const std::string& itemName) const {
        for (const auto& item : inventory) {
            if (item.getName() == itemName) return true;
        }
        return false;
    }

    virtual void attack(Character* target) {
        std::cout << name << " attacks " << target->getName() << " with a punch!" << std::endl;
        target->takeDamage(10); // Example damage
    }

    std::string getName() const { return name; }
    int getHealth() const { return health; }
    // Added for inventory management
    const std::vector<Item>& getInventory() const { return inventory; }
};

#endif
