
#ifndef ITEM_H
#define ITEM_H

#include <string>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}
    
    void interact() {
        std::cout << "Interacting with " << name << std::endl;
    }
    
    std::string getName() const { return name; }
    std::string getDescription() const { return description; }

  
};


#endif
