#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "player.h" 
#include "enemy.h"


int main() {
    Room startRoom("You are in a dimly lit room.");
    Room hallway("You are in a long hallway.");
    Room treasureRoom("You have entered a treasure room!");
    Room trapRoom("You have entered a trap room! You have 5 seconds to leave or you get eliminated.");

    startRoom.addExit("north", &hallway);
    hallway.addExit("south", &startRoom);
    hallway.addExit("east", &trapRoom);
    hallway.addExit("north", &treasureRoom);
    treasureRoom.addExit("south", &hallway);

    Item key("Key", "A shiny key that looks important.");
    Item sword("Sword", "A sharp sword with a golden hilt.");

    startRoom.addItem(key);
    treasureRoom.addItem(sword);

    Player player("Alice", 100);
    player.setLocation(&startRoom);

  // Encounter with enemy in the hallway
  if (player.getLocation() == &hallway) {
    std::cout << "An enemy appears in the hallway!" << std::endl;
    std::cout << "Do you want to 'attack' or 'run'?" << std::endl;
    std::string action;
    std::cin >> action;
    if (action == "run") {
        player.setLocation(&startRoom);
        std::cout << "You successfully escape back to the dimly lit room." << std::endl;
    } else if (action == "attack") {
        // Assuming Enemy class inherits from Character and has an attack method
        Enemy enemy("Hallway Monster", 50); 
        enemy.attack(&player);
      player.takeDamage(50);  // Player loses health after defeating the enemy
        std::cout << "You defeat the enemy but lose 50 health." << std::endl;
        std::cout << "Your new health is: " << player.getHealth() << std::endl;
    } else {
        std::cout << "Invalid action. You stand frozen in fear." << std::endl;
    }
  }
  


    bool inTrapRoom = false;

    while (true) {
        std::cout << "Current Location: " << player.getLocation()->getDescription() << std::endl;

        if (inTrapRoom) {
            std::cout << "You have 5 seconds to make a decision!" << std::endl;
            std::cout << "Enter 'leave' to try and exit the trap room, or anything else to stay:" << std::endl;
            std::string decision;
            std::cin >> decision;
            if (decision == "leave") {
                player.setLocation(&hallway);
                std::cout << "You barely escape back to the hallway!" << std::endl;
                inTrapRoom = false;
                continue;
            } else {
                std::cout << "Game Over: You were eliminated by the trap!" << std::endl;
                break;
            }
        }

        if (player.getLocation() == &trapRoom) {
            inTrapRoom = true;
        }

        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : player.getLocation()->getItems()) {
            std::cout << "- " << item.getName() << ": " << item.getDescription() << std::endl;
        }

        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Say Something | ";
        std::cout << "5. View Health | "; // New option for viewing health
        std::cout << "6. Quit" << std::endl; // Moved Quit option to number 6

        int choice;
        std::cin >> choice;
        if (choice == 1) {
            std::cout << "You look around the room, and nothing changes." << std::endl;
        } else if (choice == 2) {
            std::cout << "Enter the name of the item you want to interact with: ";
            std::string itemName;
            std::cin >> itemName;
            bool found = false;
            for (Item& item : player.getLocation()->getItems()) {
                if (item.getName() == itemName) {
                    item.interact();
                    found = true;
                    break;
                }
            }
            if (!found) {
                std::cout << "Item not found in the room." << std::endl;
            }
        } else if (choice == 3) {
            std::cout << "Enter the direction (e.g., north, south, east or west): ";
            std::string direction;
            std::cin >> direction;
            Room* nextRoom = player.getLocation()->getExit(direction);
            if (nextRoom != nullptr) {
                player.setLocation(nextRoom);
                  std::cout << "You move to the next room." << std::endl;
                  } else {
                  std::cout << "You can't go that way." << std::endl;
                  }
                  } else if (choice == 4) {
                  std::cout << "You say: Is anybody here?" << std::endl;
                  if (player.getLocation()->getDescription() == "You are in a dimly lit room.") {
                  std::cout << "You hear a faint scream echoing through the darkness." << std::endl;
              if (player.getLocation()->getDescription() == "You are in a long hallway.") {
                    std::cout << "You hear footsteps approaching you." << std::endl;
                  }
        }
                  } else if (choice == 5) {
                  // Newly added option to view player's health
                  std::cout << "Your health is: " << player.getHealth() << std::endl;
                  } else if (choice == 6) {
                  std::cout << "Goodbye" << std::endl;
                  break; // Exits the game loop, effectively ending the game
                  } else {
                  std::cout << "Invalid choice. Try again." << std::endl;
                  }
                  }
                  return 0;
                  }